//
//  AppDelegate.m
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import "AppDelegate.h"
#import "HomePageViewController.h"
#import "HCMainViewController.h"
#import "CKNavigationController.h"
#import "CKLoginViewController.h"
#import "FMDatabase.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UINavigationBar appearance].barTintColor = [UIColor colorWithRed:44/255.0 green:50/255.0 blue:57/255.0 alpha:1.0];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    // 创建/读取本地数据库
    NSString *path            = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject] stringByAppendingPathComponent:@"datafmdb.sqlite"];
    FMDatabase *database      = [FMDatabase databaseWithPath:path];

    BOOL success              =  [database open];

    int num = 0;
    if (success) {
        NSLog(@"AppDelegate打开数据库成功!");

        NSString * sql           = @"SELECT setButtonState  FROM t_mainSeting ;";
        FMResultSet *result      = [database executeQuery:sql];
        while ([result next]) {
            
            num     = [result stringForColumnIndex:0].intValue;
        }
    }else{
        NSLog(@"AppDelegate打开数据库失败!");
    }
    
//    if (num == 0) {
        CKLoginViewController *vc = [[CKLoginViewController alloc] initWithNibName:@"CKLoginViewController" bundle:[NSBundle bundleForClass:[CKLoginViewController class]]];
        self.window.rootViewController = [[CKNavigationController alloc]initWithRootViewController:vc];
//    } else {
//        // 点击过主页面的开始喝水
//        self.window.rootViewController = [[HCMainViewController alloc] init];
//
//    }
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
