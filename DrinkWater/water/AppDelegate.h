//
//  AppDelegate.h
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end

