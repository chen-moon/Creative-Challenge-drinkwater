//
//  HcTextInputView.h
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
#define mDevice  ([[[UIDevice currentDevice] systemVersion] floatValue])

@protocol HcTextInputViewDelegate <NSObject>

@optional
- (void)inputViewDelegate ;


@end


@interface HcTextInputView : UIView
@property (nonatomic, assign) id <HcTextInputViewDelegate> delegate;
@property (nonatomic, strong) UITextField *inputfeild;
@property (nonatomic, strong) UILabel *unit;
- (instancetype)initWithFrame:(CGRect)frame withType:(NSString *)type;
@end

NS_ASSUME_NONNULL_END
