//
//  UIBarButtonItem+YJWExtension.h
//  ZhaoChe
//
//  Created by YJW on 2020/6/4.
//  Copyright © 2020 YJW. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIBarButtonItem (YJWExtension)

/**创建图标item*/
+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action;

/**创建文字item，可修改文字颜色大小*/
+ (instancetype)itemWithTittle:(NSString *)tittle tittleColor:(UIColor *)color tittleFont:(UIFont *)font target:(id)target action:(SEL)action;

@end

NS_ASSUME_NONNULL_END
