//
//  UIBarButtonItem+YJWExtension.m
//  ZhaoChe
//
//  Created by YJW on 2020/6/4.
//  Copyright © 2020 YJW. All rights reserved.
//

#import "UIBarButtonItem+YJWExtension.h"

@implementation UIBarButtonItem (YJWExtension)

+ (instancetype)itemWithImage:(NSString *)image highImage:(NSString *)highImage target:(id)target action:(SEL)action{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    if (highImage) {
       [btn setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    }
//    [btn sizeToFit];
    
    btn.bounds = (CGRect){CGPointZero,CGSizeMake(30, 30)};
    if ([image isEqualToString:@"top_back"]) {
        btn.bounds = (CGRect){CGPointZero,CGSizeMake(50, 30)};
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, -35, 0, 0);
    }
    
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
    
}


+ (instancetype)itemWithTittle:(NSString *)tittle tittleColor:(UIColor *)color tittleFont:(UIFont *)font target:(id)target action:(SEL)action{
    
     UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
     [btn setTitle:tittle forState:UIControlStateNormal];
     [btn setTitleColor:color forState:UIControlStateNormal];
     [btn sizeToFit];
     [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
     return [[UIBarButtonItem alloc] initWithCustomView:btn];
}



@end
