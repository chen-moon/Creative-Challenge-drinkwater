//
//  HCMainViewController.h
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HCMainViewController : UIViewController
@property (nonatomic,copy) NSString *username;
@end

NS_ASSUME_NONNULL_END
