//
//  CKLoginViewController.h
//  ck-door
//
//  Created by YJW on 2020/12/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CKLoginViewController : UIViewController

@property (strong, nonatomic) UIViewController *homePageVC;

@end

NS_ASSUME_NONNULL_END
