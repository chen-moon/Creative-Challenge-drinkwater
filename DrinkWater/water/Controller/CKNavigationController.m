//
//  CTNavigationController.m
//  ZhaoChe
//
//  Created by YJW on 2020/6/4.
//  Copyright © 2020 YJW. All rights reserved.
//

#import "CKNavigationController.h"
#import "UIImage+YJWExtension.h"
#import "UIBarButtonItem+YJWExtension.h"
#define CKRGBColor(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define PFSCSemibold(s) [UIFont fontWithName:@"PingFangSC-Semibold" size:s]
@interface CKNavigationController ()

@end

@implementation CKNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationBar.barStyle = UIBarStyleBlack;
    [self.navigationBar setBackgroundImage:[UIImage ck_imageWithColor:CKRGBColor(80, 200, 211)] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.translucent = NO;
    [self.navigationBar setTitleTextAttributes:@{NSFontAttributeName : PFSCSemibold(22.5),
                                                NSForegroundColorAttributeName : [UIColor whiteColor],
                                                 }];
}

//可以在这个方法中拦截所有push进来的控制器
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    //恢复导航栏
    
    if (self.childViewControllers.count) {//如果push进来的不是第一个控制器
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:@"top_back" highImage:@"" target:self action:@selector(back)];
    }
    [super pushViewController:viewController animated:animated];
    
    // 修正push控制器tabbar上移问题
}

-(void)back{
    [self popViewControllerAnimated:YES];
}


@end
