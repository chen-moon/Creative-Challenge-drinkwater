//
//  CKLoginViewController.m
//  ck-door
//
//  Created by YJW on 2020/12/17.
//

#import "CKLoginViewController.h"
#import "HomePageViewController.h"
#import "CKCornerButton.h"

@interface CKLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *tfAcc;
@property (weak, nonatomic) IBOutlet UITextField *tfPw;
@property (weak, nonatomic) IBOutlet CKCornerButton *btnLogin;

@end

@implementation CKLoginViewController

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUp];
}


- (void)setUp{
    //登陆按钮
    [self.btnLogin setTitle:@"登陆" forState:(UIControlStateNormal)];
}

- (IBAction)login:(id)sender {
    if(([self.tfAcc.text isEqualToString:@"test"]) && ([self.tfPw.text isEqualToString:@"123456"])){
        HomePageViewController *vc = [[HomePageViewController alloc]init];
        vc.username = self.tfAcc.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
  

}

@end
