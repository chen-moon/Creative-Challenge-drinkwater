//
//  UIImage+YJWExtension.h
//  ZhaoChe
//
//  Created by YJW on 2020/6/4.
//  Copyright © 2020 YJW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (YJWExtension)

/**颜色转换成UIImage类型*/ 
+ (UIImage *)ck_imageWithColor:(UIColor *)color;

/**给图片切圆角*/
- (UIImage *)ck_drawRectWithRoundedCorner;

@end


