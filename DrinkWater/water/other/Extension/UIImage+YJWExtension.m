//
//  UIImage+YJWExtension.m
//  ZhaoChe
//
//  Created by YJW on 2020/6/4.
//  Copyright © 2020 YJW. All rights reserved.
//

#import "UIImage+YJWExtension.h"

@implementation UIImage (YJWExtension)

+ (UIImage *)ck_imageWithColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, 1.0, 1.0);
    //创建基于位图的图形上下文并使其成为当前上下文
    UIGraphicsBeginImageContext(rect.size);
    //创建当前图形上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //使用CGColor设置图形上下文中的当前填充颜色。
    CGContextSetFillColorWithColor(context, [color CGColor]);
    //使用当前图形状态中的填充颜色绘制所提供矩形中包含的区域
    CGContextFillRect(context, rect);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}


- (UIImage *)ck_drawRectWithRoundedCorner{
    //NO 代表透明
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0.0);
    //获得上下文
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //添加一个圆
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextAddEllipseInRect(ctx, rect);
    //剪裁
    CGContextClip(ctx);
    //将图片画上去
    [self drawInRect:rect];
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

@end
