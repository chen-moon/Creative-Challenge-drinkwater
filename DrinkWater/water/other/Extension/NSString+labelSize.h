//
//  NSString+labelSize.h
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NSString (labelSize)
- (CGSize)textSizewithFont:(UIFont *)font andMaxSize:(CGSize)maxSize;
@end

NS_ASSUME_NONNULL_END
