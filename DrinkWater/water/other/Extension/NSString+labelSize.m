//
//  NSString+labelSize.m
//  water
//
//  Created by chck999 on 2021/8/24.
//

#import "NSString+labelSize.h"

@implementation NSString (labelSize)

- (CGSize)textSizewithFont:(UIFont *)font andMaxSize:(CGSize)maxSize {
    
    NSDictionary * attr = @{NSFontAttributeName : font};
    
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attr context:nil].size;
}
@end
